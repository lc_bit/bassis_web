package bassis.bassis_web.assist;

public class Resource {
	private ServletAttribute servletAttribute;
	private ServletClient servletClient;
	private ServletCookie servletCookie;
	private ServletResource servletResource;

	public ServletAttribute getServletAttribute() {
		return servletAttribute;
	}

	public void setServletAttribute(ServletAttribute servletAttribute) {
		this.servletAttribute = servletAttribute;
	}

	public ServletClient getServletClient() {
		return servletClient;
	}

	public void setServletClient(ServletClient servletClient) {
		this.servletClient = servletClient;
	}

	public ServletCookie getServletCookie() {
		return servletCookie;
	}

	public void setServletCookie(ServletCookie servletCookie) {
		this.servletCookie = servletCookie;
	}

	public ServletResource getServletResource() {
		return servletResource;
	}

	public void setServletResource(ServletResource servletResource) {
		this.servletResource = servletResource;
	}

	public Resource(ServletAttribute servletAttribute, ServletClient servletClient, ServletCookie servletCookie,
			ServletResource servletResource) {
		super();
		this.servletAttribute = servletAttribute;
		this.servletClient = servletClient;
		this.servletCookie = servletCookie;
		this.servletResource = servletResource;
	}

}
