package bassis.bassis_web;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import bassis.bassis_bean.BeanFactory;
import bassis.bassis_bean.IocFactory;
import bassis.bassis_bean.ReferenceDeclaration;
import bassis.bassis_bean.proxy.BeanInvoke;
import bassis.bassis_tools.exception.CustomException;
import bassis.bassis_tools.gc.GcUtils;
import bassis.bassis_tools.properties.ReadProperties;
import bassis.bassis_tools.string.StringUtils;
import bassis.bassis_web.annotation.impl.ControllerImpl;
import bassis.bassis_web.annotation.impl.InterceptorImpl;
import bassis.bassis_web.assist.Resource;
import bassis.bassis_web.assist.ServletAttribute;
import bassis.bassis_web.assist.ServletClient;
import bassis.bassis_web.assist.ServletCookie;
import bassis.bassis_web.assist.ServletResource;
import bassis.bassis_web.assist.ServletView;
import bassis.bassis_web.assist.StewardResource;
import bassis.bassis_web.work.Page;
import bassis.bassis_web.work.ResultBean;
import bassis.bassis_web.work.View;

/**
 * Servlet implementation class ContainerServlet
 */
public class BassisContainer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static ServletConfig servletConfig;
	static ServletContext servletContext;
	static ReadProperties properties;
	static final Logger logger = Logger.getLogger(BassisContainer.class);
	static Map<String, Class<?>> mapActions = new HashMap<String, Class<?>>();
	static Map<String, Method> mapMethods = new HashMap<String, Method>();
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		servletConfig = config;
		// 获得上下文
		servletContext = config.getServletContext();
		//获得读取器
		properties=BeanFactory.getProperties();
		// 获得默认properties配置
		properties.read(ReferenceDeclaration.SERVLET_PROPERTIES);
		// 获得web配置参数
		String contextConfigLocation = config.getInitParameter(ReferenceDeclaration.CONTEXTCONFIGLOCATION);
		if (StringUtils.isEmptyString(contextConfigLocation)) {
			CustomException.throwOut("init servlet failure : parameter [ " + ReferenceDeclaration.CONTEXTCONFIGLOCATION
					+ " ] not allowed");
			return;
		}
		properties.read(contextConfigLocation);
		// 校验必须参数
		if (!ReadProperties.verification(ReferenceDeclaration.INIT_PARAMETER.split(","))) {
			CustomException.throwOut("init servlet failure : INIT_PARAMETER not allowed [ "
					+ ReferenceDeclaration.INIT_PARAMETER + " ]");
			return;
		}
		// 初始化核心参数
		ReferenceDeclaration.setControllerPackage(properties.getProperty(ReferenceDeclaration.CONTROLLERPACKAGE));
		ReferenceDeclaration.setControllerSuffix(properties.getProperty(ReferenceDeclaration.CONTROLLERSUFFIX));
		// 项目名称（需要做个寻址过滤器，为自定义访问路径提供注册功能）
		String contextPath = servletContext.getContextPath();
		ReferenceDeclaration.setProjectRoot(contextPath.substring(1));
		// 扫描路径 默认为当前项目路径下 取当前顶级包名和次级包名作为扫描路径
		String scanRoot = StringUtils.indexOf_str(this.getClass().getName(), ".", 2);
		if (ReadProperties.verification(ReferenceDeclaration.SCANROOT)) {
			scanRoot = properties.getProperty(ReferenceDeclaration.SCANROOT);
		}
		// 当所有扫描路径都不可用时 选择控制器根目录进行扫描，这是非常危险的，可能会忽略资源注解
		if (StringUtils.isEmptyString(scanRoot)) {
			scanRoot = ReferenceDeclaration.getControllerPackage();
		}
		ReferenceDeclaration.setScanRoot(scanRoot);
		// 启动bean工厂
		BeanFactory.init();
		// 启动Controller注解分析
		ControllerImpl.getInstance();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.debug("service方法开始");
		request.setCharacterEncoding("utf-8");
		ServletAttribute servletAttribute = ServletAttribute.init(servletContext, request, response);
		ServletResource servletResource = ServletResource.init(servletAttribute);
		ServletClient servletClient = ServletClient.init(servletAttribute);
		ServletCookie servletCookie = ServletCookie.init(servletAttribute);
		String path = servletResource.getPath();
		logger.debug("请求资源：" + path);
		Class<?> las=null;
		try {
			las = mapActions.get(path);
			Method method = mapMethods.get(path);
			if (null == las || null == method){
				initResource(servletResource);
				las = mapActions.get(path);
				method = mapMethods.get(path);
			}
			if (null == las || null == method){
				CustomException.throwOut("没有找到资源：" + path);
			}
			try {
				Resource resource = new Resource(servletAttribute, servletClient, servletCookie, servletResource);
				StewardResource.put(servletResource.getControllerPath(), resource);
				//拦截器应该在这里进行
				InterceptorImpl impl=InterceptorImpl.init();
				impl.Interceptor(las, resource);
				//进行ioc注入
				IocFactory.analyseFieldIOC(las, servletResource.getMap());
				logger.debug("IOC处理完成");
			} catch (Exception e) {
				// TODO: handle exception
				CustomException.throwOut(e.getMessage(), e);
			}
			//进行代理
			Object res =BeanInvoke.invokeMethod(las, method);
			// 初始化视图
			initView(servletAttribute, servletResource, res);
			// 处理结果
			logger.debug("IOC处理完成");
		} catch (Exception e) {
			// TODO: handle exception
			CustomException.throwOut("service init controller failure", e);
		} finally {
			clear(las,servletAttribute, servletResource, servletClient, servletCookie);
			GcUtils.getInstance();
		}
		logger.debug("service方法完成");
	}

	/**
	 * 资源清理
	 */
	private static void clear(Class<?> las,ServletAttribute servletAttribute,ServletResource servletResource,ServletClient servletClient,ServletCookie servletCookie) {
		// 清除掉这个管家对象
		StewardResource.remove(servletResource.getControllerPath());
		//移除bean工厂创建的实例
		BeanFactory.remove(las);
		servletAttribute =null;
		servletResource=null;
		servletClient =null;
		servletCookie=null;
		logger.debug("资源清理完成");
	}

	/**
	 * 配置返回视图
	 * 
	 * @param servletAttribute
	 * @param servletResource
	 * @param res
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private static void initView(ServletAttribute servletAttribute, ServletResource servletResource, Object res)
			throws Exception {
		ServletView servletView = ServletView.init(servletAttribute, servletResource);
		int viewType = servletResource.getResourceType();
		servletView.sendConfig(viewType);
		// 判定res的类型//基于方法返回的数据
		if (null == res) {
			// 没有任何输出 直接系统输出访问成功
			ResultBean r = new ResultBean(true, ServletView.SUCCESS, "操作成功", null);
			servletView.outJson(r);
			return;
		} else if (res instanceof View) {
			View v = (View) res;
			servletView.setRlt(v.getRlt());
			servletView.setSendType(v.isSendType());
			servletView.setSendUrl(v.getSendUrl());
		} else if (res instanceof String) {
			String r = (String) res;
			if (StringUtils.startsWith(r, "{") || ServletView.json == viewType) {
				// json输出
				servletView.outString(r);
				return;
			} else {
				// 转发路径
				servletView.setSendUrl(r);
			}
		}else if (res instanceof Page<?>) {//分页构造参数
			servletView.outJson_root(res);
			return;
		}else{
			//当做objjson出去
			servletView.outJson(res);
			return;
		}
		// 需要配置是转发还是重定向 需要配置转发页面 //false 表示转发 true表示重定向
		if (StringUtils.isEmptyString(servletView.getSendUrl()) && viewType != ServletView.json) {
			// 不是返回json数据且没有配置地址
			// 跳转到404页面
			servletView.setSendType(true);
			servletView.setSendUrl(ServletView.error_404);
		}
		servletView.send();
	}

	// 在这里根据请求路径去匹配注解 获得对应的控制器类
	@SuppressWarnings("unused")
	private static void initResource(ServletResource servletResource) throws Exception {
		String path = StringUtils.startslastIndexOf_str(servletResource.getPath(), ".");
		logger.debug("开始匹配控制器，筛选出可能的控制器：" + path);
		Map<String, Class<?>> waitMap = new HashMap<String, Class<?>>();
		String[] pathArr = path.substring(1).split("/");
		String cm = "";
		for (int i = 0; i < pathArr.length - 1; i++) {
			cm = cm + "/" + pathArr[i];
			Class<?> cl = ControllerImpl.getMapClass(cm);
			if (null == cl) {
				continue;
			}
			waitMap.put(cm, cl);
			break;
		}
		if (null == waitMap || waitMap.isEmpty()) {
			CustomException.throwOut("controller is  not ");
			return;
		}
		Map<String, Map<String, Method>> waitMethodMap = new HashMap<String, Map<String, Method>>();
		String actionPath = null;
		Class<?> actionClass = null;
		// 分析waitMap
		for (String key : waitMap.keySet()) {
			Map<String, Method> met = ControllerImpl.getMapMethod(key);
			if (null == met) {
				continue;
			}
			waitMethodMap.put(key, met);
			// 现在确定的请求控制器路径为key 控制器实例为waitMap.get(key);
			actionPath = key;
			actionClass = waitMap.get(key);
			servletResource.setControllerPath(actionPath);
		}
		if (null == actionClass || null == actionPath || null == waitMethodMap || waitMethodMap.isEmpty()) {
			return;
		}
		// 分析waitMethodMap
		// 得到方法路径
		String methodPath = StringUtils.replace(path, actionPath, "");
		servletResource.setMethodPath(methodPath);
		Map<String, Method> mMap = waitMethodMap.get(actionPath);
		if (!mMap.containsKey(methodPath))
			return;

		// 得到方法
		Method method = mMap.get(methodPath);
		mapActions.put(servletResource.getPath(), actionClass);
		mapMethods.put(servletResource.getPath(), method);
		logger.debug("请求资源 控制器：" + actionClass.getName() + " | 方法：" + method.getName());
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
		servletConfig = null;
		servletContext = null;
		properties = null;
		mapActions = null;
		mapMethods = null;
	}

}
